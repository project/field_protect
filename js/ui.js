'use strict';

/* globals Drupal,document */
(function (Drupal, document, once) {
  Drupal.theme.fieldProtectConfirmationMessage = (message, rememberHash) => {
    let htmlStr = '<h4>' + Drupal.t('This field is locked') + '</h4>' +
    '<p>' + Drupal.t('You can edit this field, but you need to understand the implications. This field is protected with the following message:') + '</p>' +
    '<blockquote>' + message + '</blockquote>' +
    '<p>' + Drupal.t('Are you sure you want to proceed?') + '</p>';
    if (rememberHash) {
      htmlStr += '<input class="form-boolean form-boolean--type-checkbox" id="remember--' + rememberHash + '" name="remember[' + rememberHash + ']" type="checkbox"><label class="option" for="remember--' + rememberHash + '"> ' + Drupal.t('Remember this decision') + '</label>';
    }
    return '<div>' + htmlStr + '</div>';
  }

  /**
   * @todo
   */
  Drupal.behaviors.fieldProtect = {
    attach: function (context, settings) {
      const buildOnClick = (message, rememberHash, rememberUrl) => (event) => {
        const unlockButton = event.target;
        const element = Drupal.theme('fieldProtectConfirmationMessage', message, rememberHash);
        const dialog = Drupal.dialog(element, {
          title: Drupal.t('Please confirm before you continue'),
          dialogClass: 'field-protect-ack-form',
          resizable: true,
          height: 'auto',
          width: 'auto',
          buttons: [{
            text: Drupal.t('Cancel'),
            class: 'button button--primary',
            click: function click() {
              dialog.close();
            },
          }, {
            text: Drupal.t('Unlock'),
            class: 'button button--secondary',
            click: function click() {
              const wrapper = unlockButton.parentElement.parentElement;
              wrapper.querySelectorAll('input, select, textarea').forEach(function (element) {
                element.removeAttribute('disabled');
                // Check if element has CKEditor.
                if (element.hasAttribute('data-editor-active-text-format')) {
                  const textFormat = element.getAttribute('data-editor-active-text-format');
                  const format = settings.editor.formats[textFormat];
                  // Detach and reattach the CKEditor.
                  Drupal.editorDetach(element, format, '');
                  Drupal.editorAttach(element, format);
                }
              });

              unlockButton.parentElement.remove();
              wrapper.classList.remove('field-protected');
              const remember = document.getElementById('remember--' + rememberHash);
              if (remember && remember.checked) {
                const formData = new FormData();
                formData.append('rememberHash', rememberHash);
                // Send and forget.
                fetch(rememberUrl, {
                  method: 'POST',
                  body: formData,
                  credentials: 'same-origin'
                })
              }
              dialog.close();
            },
          }],
        });
        dialog.showModal();
      }
      settings.fieldProtect = settings.fieldProtect || {};
      settings.fieldProtect.fields = settings.fieldProtect.fields || {};
      const fieldsToProcess = Object.keys(settings.fieldProtect.fields) || [];
      fieldsToProcess.map(drupalSelectorDataAttribute => {
        const fieldSetting = settings.fieldProtect.fields[drupalSelectorDataAttribute] || {};
        const wrapperSelector = `[data-drupal-selector="${drupalSelectorDataAttribute}"]`;
        const wrapper = context.querySelector(wrapperSelector);
        if (!wrapper) {
          return;
        }
        const buttons = once('field-unlock', '.unlock-element > button', wrapper);
        if (buttons.length === 0) {
          return;
        }
        // Disable protected elements.
        const elementsToProtect = wrapper.querySelectorAll('input, select, textarea') || [];
        elementsToProtect.forEach(element => element.setAttribute('disabled', '1'));
        // Attach listener to activate Field Protect modal on click.
        buttons[0].addEventListener('click', buildOnClick(
          fieldSetting.message,
          fieldSetting.rememberHash,
          settings.fieldProtect.rememberUrl,
        ));
      });
    },
  };

}(Drupal, document, once));
