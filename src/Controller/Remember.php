<?php

namespace Drupal\field_protect\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Remember extends ControllerBase {

  const FIELD_PROTECT_STATE_KEY = 'field_protect.remembered';

  /**
   * @var \Drupal\Core\State\StateInterface
   */
  private $state;

  /**
   * @param \Drupal\Core\State\StateInterface $state
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  public static function create(ContainerInterface $container) {
    return new static($container->get('state'));
  }

  public function invoke(Request $request): Response {
    $remember_hash = $request->get('rememberHash');
    $remembered = $this->state->get(Remember::FIELD_PROTECT_STATE_KEY, []);
    $remembered[] = $remember_hash;
    $this->state->set(Remember::FIELD_PROTECT_STATE_KEY, $remembered);
    return new Response(NULL, Response::HTTP_NO_CONTENT);
  }

}
