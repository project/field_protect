<?php

namespace Drupal\field_protect\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Field Protect settings for this site.
 */
class SettingsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'field_protect_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['info'] = [
      '#markup' => '<p>' . $this->t('Click "Forget" to show fields as locked again, even after editors chose "Remember this decision".') . '</p>',
    ];
    $form['actions']['forget'] = [
      '#type' => 'submit',
      '#value' => $this->t('Forget'),
      '#description' => $this->t('Clear remembered unlocks.'),
      '#name' => 'forget',
      '#button_type' => 'danger',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $element = $form_state->getTriggeringElement();
    $name = $element['#name'] ?? '';
    if ($name !== 'forget') {
      return;
    }
    \Drupal::state()->set('field_protect.remembered', []);
    $this->messenger()->addStatus($this->t('All fields will show as locked again.'));
  }

}
